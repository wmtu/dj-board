// JavaScript Clock
function clock()
{
    var DateTime = luxon.DateTime;

    document.getElementById("clock").textContent = DateTime.utc().setZone('America/New_York').toFormat('cccc, LLLL d, yyyy h:mm a'); //toLocaleString(DateTime.DATETIME_MED);
    
    setTimeout(clock, 1000);
}

clock();