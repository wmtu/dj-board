// update statuspage information

// statuspage colors
// green: #2fcc66
// yellow: #f1c40f
// orange: #e67e22
// red: #e74c3c
// blue: #3498DB
var indicators = 
    [
        {type: "none",        'color': "#2fcc66"},
        {type: "minor",       'color': "#f1c40f"},
        {type: "major",       'color': "#e67e22"},
        {type: "maintenance", 'color': "#3498DB"}
    ];

function statusBuilder(data)
{
    // get the current status
    var status = data['status']['description'];
    var type = data['status']['indicator'];

    // set status color
    indicators.forEach((item, index) => {
        if (item.type == type)
            { document.getElementById('wmtu_status').style.backgroundColor = indicators[index].color; } });
    
    // set status text
    document.getElementById('wmtu_status_desc').innerText = status;
}

// query the WMTU statuspage.io api endpoint
function queryStatus(interval)
{
    // get statuspage status info
    var query_url = "https://n0pg6brf8r04.statuspage.io/api/v2/status.json";
    fetch(query_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { statusBuilder(data); })
        .catch(function(err) { console.log(err); });

    setTimeout(queryStatus, interval*1000, interval);
}

queryStatus(interval_long);