// song parser to display the song list

// variables to save current song and album to
var c_song    = "";
var c_artist  = "";
var c_album   = "";

function playlistBuilder(data)
{
    // check if it's the same song, if not then build the playlist
    if (data[0]['song'] != c_song || data[0]['artist'] != c_artist || data[0]['album'] != c_album)
    {
        // set current data
        c_song    = data[0]['song'];
        c_artist  = data[0]['artist'];
        c_album   = data[0]['album'];

        // set np artwork image
        var art_url = data[0]['artwork'];
        if ( art_url == "") { art_url = "img/wmtu_art.png"; }
        document.getElementById("np_art").setAttribute("src", art_url);
        // set np text info
        document.getElementById("np_song").innerHTML = c_song + "&nbsp;";
        document.getElementById("np_artist").innerHTML = c_artist + "&nbsp;";
        document.getElementById("np_album").innerHTML = c_album + "&nbsp;";

        // update the recently played songs
        // rp artwork
        var rp_art_nodes = document.querySelectorAll('.rp_art');
        rp_art_nodes.forEach((element, index) => {
            var img_url = data[index + 1]['artwork'];
            if (img_url == "") { img_url = "img/wmtu_art.png"; }
            element.setAttribute('src', img_url);
        });

        // rp text info
        var rp_song_nodes = document.querySelectorAll('.rp_song');
        rp_song_nodes.forEach((element, index) => {
            element.innerHTML = data[index + 1]['song'] + "&nbsp;"; });

        var rp_artist_nodes = document.querySelectorAll('.rp_artist');
        rp_artist_nodes.forEach((element, index) => {
            element.innerHTML = data[index + 1]['artist'] + "&nbsp;"; });

        var rp_album_nodes = document.querySelectorAll('.rp_album');
        rp_album_nodes.forEach((element, index) => {
            element.innerHTML = data[index + 1]['album'] + "&nbsp;"; });
    }
}

// queries the API with specified values
function queryAPI(n, interval)
{
    var log_endpoint = "https://log.wmtu.fm/api/2.0/history";
    var query_args = {'type': 'song', 'n': n};
    var arg_keys = Object.keys(query_args);

    // build full query url string
    var arg_string = "?"
    arg_keys.forEach((arg, index) => {
        arg_string = arg_string + `${arg}=${query_args[arg]}` + "&";});
    arg_string = arg_string.substring(0, arg_string.length - 1);
    var query_url = log_endpoint + arg_string;

    // fetch json data from the API
    // then send it to the view builder
    fetch(query_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { playlistBuilder(data); })
        .catch(function(err) { console.log(err); });

    var interval_secs = interval * 1000;
    setTimeout(queryAPI, interval_secs, n, interval);
}

// query the api for new songs
queryAPI(10, interval_short);