// from MTU
window.addEventListener('load',function() {
    loadJS = function (url,loadedFunc) {
        var script = document.createElement('script');
        script.src = url;
        if(loadedFunc) { script.onload = loadedFunc; }
        document.head.appendChild(script);
    }
	
	/*
		FOR STREAMING CAMS	
	*/
	
    if (document.getElementById('cam')) 
    {
        loadJS("js/jwplayer.js",loaded);
        
        function loaded()
        {
            jwplayer.key="4kn1ZxArnliee1oIEyBOrDlkSuOviRjj3Awt9rGkOVw=";
            var datafile = $('#cam').data('file');
            
            var ratio = $('#cam').data('ratio');
            if (ratio==null) ratio = '16:9';
            if (datafile==null) 
            {
                jwplayer('cam').setup({
                    playlist: [{
                        sources: [
                            {file: "https://streamingwebcams.mtu.edu:1935/rtplive/camera"+($('#cam').attr('class').substring(6))+".stream/playlist.m3u8"},
                            {file: "https://streamingwebcams.mtu.edu:1935/rtplive/camera"+($('#cam').attr('class').substring(6))+".stream/manifest.mpd"},
                            {file: "rtmp://streamingwebcams.mtu.edu:1935/rtplive/camera"+($('#cam').attr('class').substring(6))+".stream"}
                        ]
                    }],	
                    type: 'mp4',
                    primary: 'html5',
                    width: '100%',
                    aspectratio: ratio,
                    autostart: true,
                    hlshtml: true,
                    ga: {},
                    logo: {
                        file: 'img/stream_logo.png',
                        hide: true}
                });
                
                jwplayer().on('error', function(event) {
                    ga('send', 'event', 'JW Player Events', 'Errors', event.code+': '+event.message);
                    console.log('JW Player Error: ' + event.code+' - '+event.message);
                });   
            }
            else 
            {
                jwplayer('cam').setup({
                    file: datafile,
                    type: 'mp4',
                    primary: 'html5',
                    width: '100%',
                    aspectratio: ratio,
                    autostart: true,
                    hlshtml: true,
                    ga: {},
                    logo: {
                        file: 'img/stream_logo.png',
                        hide: true}
                }); 
            }

            if (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent))
            {
                $("#cam").before("<h3>Notice: Streaming not supported in older versions of Internet Explorer. Please update your browser to the latest version.</h3>");
            }
        }
	}
});
