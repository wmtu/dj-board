// fetch the weather and display it

// day titles
var days =
[
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
];

// color codes for weather types
var colors =
    [
        { code: 0,  color: "#ff5722" }, // tornado
        { code: 1,  color: "#ffeb3b" }, // tropical storm
        { code: 2,  color: "#ff5722" }, // hurricane
        { code: 3,  color: "#ffeb3b" }, // severe thunderstorms
        { code: 4,  color: "#303f9f" }, // thunderstorms
        { code: 5,  color: "#85c1e9" }, // mixed rain and snow
        { code: 6,  color: "#85c1e9" }, // mixed rain and sleet
        { code: 7,  color: "#eceff1" }, // mixed snow and sleet
        { code: 8,  color: "#80deea" }, // freezing drizzle
        { code: 9,  color: "#1976d2" }, // drizzle
        { code: 10, color: "#80deea" }, // freezing rain
        { code: 11, color: "#1976d2" }, // showers
        { code: 12, color: "#1976d2" }, // showers
        { code: 13, color: "#e0f7fa" }, // snow flurries
        { code: 14, color: "#e0f7fa" }, // light snow showers
        { code: 15, color: "#e0f7fa" }, // blowing snow
        { code: 16, color: "#fafafa" }, // snow
        { code: 17, color: "#b2ebf2" }, // hail
        { code: 18, color: "#b2ebf2" }, // sleet
        { code: 19, color: "#d7ccc8" }, // dust
        { code: 20, color: "#eceff1" }, // foggy
        { code: 21, color: "#eceff1" }, // haze
        { code: 22, color: "#616a6b" }, // smoky
        { code: 23, color: "#d0d3d4" }, // blustery
        { code: 24, color: "#d0d3d4" }, // windy
        { code: 25, color: "#29b6f6" }, // cold
        { code: 26, color: "#eceff1" }, // cloudy
        { code: 27, color: "#eceff1" }, // mostly cloudy (night)
        { code: 28, color: "#eceff1" }, // mostly cloudy (day)
        { code: 29, color: "#eceff1" }, // partly cloudy (night)
        { code: 30, color: "#eceff1" }, // partly cloudy (day)
        { code: 31, color: "#4527A0" }, // clear (night)
        { code: 32, color: "#fbc02d" }, // sunny
        { code: 33, color: "#1976d2" }, // fair (night)
        { code: 34, color: "#1976d2" }, // fair (day)
        { code: 35, color: "#85c1e9" }, // mixed rain and hail
        { code: 36, color: "#d32f2f" }, // hot
        { code: 37, color: "#303f9f" }, // isolated thunderstorms
        { code: 38, color: "#303f9f" }, // scattered thunderstorms
        { code: 39, color: "#303f9f" }, // scattered thunderstorms
        { code: 40, color: "#1976d2" }, // scattered showers
        { code: 41, color: "#fdfefe" }, // heavy snow
        { code: 42, color: "#fafafa" }, // scattered snow showers
        { code: 43, color: "#fdfefe" }, // heavy snow
        { code: 44, color: "#eceff1" }, // partly cloudy
        { code: 45, color: "#303f9f" }, // thundershowers
        { code: 46, color: "#fafafa" }, // snow showers
        { code: 47, color: "#303f9f" }, // isolated thunderstorms
        { code: 3200, color: "#000000" }
    ];

// parse the weather info and build out the view
function weatherBuilder(data)
{
    var DateTime = luxon.DateTime;

    // set current weather conditions
    var c_temp   = data.current_observation.condition.temperature;
    var c_status = data.current_observation.condition.text;
    var c_code   = data.current_observation.condition.code;
    var c_text   = "It is currently " + c_temp + "° F and " + c_status;
    document.getElementById('weather').style.backgroundColor = colors[c_code].color;
    document.getElementById('current_weather').textContent = c_text;

    var fcst_nodes = document.querySelectorAll('.fcst_item');
    fcst_nodes.forEach((element, index) => {
        element.firstElementChild.innerHTML = DateTime.fromSeconds(data['forecasts'][index]['date']).setZone('America/New_York').toLocaleString({weekday: 'long'});
        element.firstElementChild.nextElementSibling.innerHTML = '<i class="wi wi-yahoo-' + data['forecasts'][index]['code'] + '"></i>';
        element.firstElementChild.nextElementSibling.nextElementSibling.innerHTML = data['forecasts'][index]['high'] + "°";
        element.lastElementChild.innerHTML = data['forecasts'][index]['low'] + "°";
    });
}

// fetch weather data from our weather proxy
function queryWeather(interval)
{
    var query_url = "https://proxy.wmtu.fm/weather/forecast.json";

    fetch(query_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { weatherBuilder(data); })
        .catch(function(err) { console.log(err); });

    var secs = interval * 1000;
    setTimeout(queryWeather, secs, interval);
}

queryWeather(interval_long);