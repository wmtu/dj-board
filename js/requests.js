// build the view for twitter requests
function twitReqBuilder(tweets)
{
    var DateTime = luxon.DateTime;

    var tweet_nodes = document.querySelectorAll('.tweet');
    tweet_nodes.forEach((element, index) => {
        element.firstElementChild.innerHTML = "From @" + tweets[index].user.screen_name;
        element.firstElementChild.nextElementSibling.innerHTML = DateTime.fromFormat(tweets[index].created_at, "EEE MMM dd HH:mm:ss ZZZ yyyy", {zone: 'Etc/UTC'}).setZone('America/New_York').toFormat("MMM d, h:mm a");
        element.lastElementChild.innerHTML = tweets[index].text;
    });
}

// build the view for requests sent to our API
function logReqBuilder(requests)
{
    var DateTime = luxon.DateTime;

    var req_nodes = document.querySelectorAll('.request');
    req_nodes.forEach((element, index) => {
        element.firstElementChild.innerHTML = "From " + requests[index].rq_name;
        element.firstElementChild.nextElementSibling.innerHTML = DateTime.fromFormat(requests[index].timestamp, "yyyy-MM-dd HH:mm:ss", {zone: "America/New_York"}).toFormat("MMM d, h:mm a");
        element.firstElementChild.nextElementSibling.nextElementSibling.firstElementChild.innerHTML = requests[index].song;
        element.firstElementChild.nextElementSibling.nextElementSibling.lastElementChild.innerHTML = requests[index].artist;
        element.lastElementChild.innerHTML = requests[index].rq_message;
    });
}

// fetch the current song requests
function queryRequests(interval)
{
    // get twitter requests
    var tweets_url = "https://proxy.wmtu.fm/twitter/requests.json";
    fetch(tweets_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { twitReqBuilder(data); })
        .catch(function(err) { console.log(err); });

    var log_req_url = "https://log.wmtu.fm/api/2.0/history?type=request&n=5"
    fetch(log_req_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { logReqBuilder(data); })
        .catch(function(err) { console.log(err); });

    setTimeout(queryRequests, interval*1000, interval);
}

queryRequests(interval_med);