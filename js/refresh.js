// refreshes the page every so often
function refreshPls()
{
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();

    var hours = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22];
    for ( var i = 0; i < hours.length; i++ )
    {
        if ( h == hours[i] && m == 0 && s == 0 )
            window.location.reload(true);
    }

    // runs every second
    setTimeout(refreshPls, 1000);
}

refreshPls();