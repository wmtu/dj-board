// script to fetch the current listener counts

// update the stream listener count
function listenerBuilder(icecast)
{
    var icecast_listeners = -1;
    icecast['icestats']['source'].forEach((stream, index) => {
        icecast_listeners += stream['listeners'];
    });
    document.getElementById('listeners_audio').innerText = "Listeners: " + icecast_listeners;
}

// update the stream viewer count
function viewerBuilder(stats)
{
    document.getElementById('listeners_video').innerText = "Viewers: " + stats['data'][0]['viewer_count'];
}

// query the endpoints and send data to the view builder
function queryListeners(interval)
{
    // get icecast stream info
    var icecast_url = "https://stream.wmtu.fm/status-json.xsl";
    fetch(icecast_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { listenerBuilder(data); })
        .catch(function(err) { console.log(err); }); 

    // get twitch stream info
    var twitch_url = "https://proxy.wmtu.fm/twitch/stats.json";
    fetch(twitch_url)
        .then(function(response) { return response.json(); })
        .then(function(data) { viewerBuilder(data); })
        .catch(function(err) { console.log(err); });

    setTimeout(queryListeners, interval*1000, interval);
}

queryListeners(interval_med);