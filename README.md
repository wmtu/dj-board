# DJ Board

A web app for DJs to use during their shows with song information, weather, and listener counts.
Formatted for a 1080p screen like we use in our DJ booth.

Check it out at <https://dj.wmtu.fm>

